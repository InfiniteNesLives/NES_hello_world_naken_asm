
#Build directory
BUILD = build

#include file directory
INCLUDE=-I ./include -I ./source

#source file directory
#must manually list out all the file names in objects dependency below
SOURCE_DIR=source

#project name
#doesn't need to be associated with any file names
PROJ = inltest

ASM = naken_asm

AFLAGS= -l	#create .lnk file 
#AFLAGS+= -q	#quiet, errors only
#AFLAGS+= -extraflagshere

#DEFINE= -mstm8

#OUTTYPE= -h	#output hex
#OUTPOST=hex
#OUTTYPE= -e	#output elf
#OUTPOST=elf
OUTTYPE= -b	#output bin
OUTPOST=nes

AFLAGS+= $(DEFINE) $(INCLUDE) 

all: dir
	$(ASM) -o $(BUILD)/$(PROJ).$(OUTPOST) $(OUTTYPE) $(AFLAGS) $(SOURCE_DIR)/proj.asm

PRGMR= stlinkv2
DEVICE= stm8s003f3

program: all
	stm8flash -c $(PRGMR) -p $(DEVICE) -s flash -w $(BUILD)/$(PROJ).$(OUTPOST)

disasm:
	naken_util -disasm -stm8 $(BUILD)/$(PROJ).$(OUTPOST)

dir:
	mkdir -p $(BUILD)

clean: 
	rm -rf $(BUILD)
