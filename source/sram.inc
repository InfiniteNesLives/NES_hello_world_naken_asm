;2KB - 256Bytes of Zero Page = 1792Bytes total
;7 pages of system RAM on NES
;the stack is able to use page 1 at most 
;So there are 6 pages (1536B) of SRAM that aren't consumed by ZP or stack
;256B stack is pretty large though, so feel free to limit the stack size to 
;gain more heap space.

;nesdev sample SRAM map: https://wiki.nesdev.com/w/index.php/Sample_RAM_map
;$0000-$000F 	16 bytes 	Local variables and function arguments
;$0010-$00FF 	240 bytes 	Global variables accessed most often, including certain pointer tables
;$0100-$019F 	160 bytes 	Data to be copied to nametable during next vertical blank (see The frame and NMIs)
;$01A0-$01FF 	96 bytes 	Stack
;$0200-$02FF 	256 bytes 	Data to be copied to OAM during next vertical blank
;$0300-$03FF 	256 bytes 	Variables used by sound player, and possibly other variables
;$0400-$07FF 	1024 bytes 	Arrays and less-often-accessed global variables 

.org 0x0100
stack:
		;$0100-01FF STACK only page 1 can be used for the decending stack.  
		;On reset it should be set to it's maximum (0xFF), and each push decrements.
		;stack rollover will wrap around and corrupt previous values at the 'bottom' of the stack
		;6502 uses "empty stack" so SP points to next position to store data
.org 0x01FF
stack_end:
.define STACK_MAX	0xFF

.org 0x0200
oam:		.resb	256

.org 0x0300
nt_buff:	.resb	256

.org 0x0400
pal_buff:	.resb	32

;delay10: 	.resb 1
;cic_ram: 	.resb 32
;.define key_ram		cic_ram
;.define lock_ram	cic_ram+16
;.define	num_mangle 	cic_ram+0	;save a byte! reuse i/o byte

;region: 	.resb 1
;xfr_cnt: 	.resb 1
;attempts: 	.resb 1
;data_dir:	.resb 1		;this is equivalent of segher's P3.0 but inverted
;data_in_mask:	.resb 1
;next_tim1_arr:	.resb 2
;.define next_tim1_arr_h next_tim1_arr
;.define next_tim1_arr_l next_tim1_arr+1	;big endian
;a few sram byte/word variables
;varB0: .resb 1
;varB1: .resb 1
;varW0: .resb 2
;varW1: .resb 2

;clock_det:	resb 32		;polling CIC CLK looking for 4Mhz clock signal


