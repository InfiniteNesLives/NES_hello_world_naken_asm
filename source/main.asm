
palette_data:
.db $0F,$15,$26,$37 ; bg0 purple/pink
.db $0F,$09,$19,$29 ; bg1 green
.db $0F,$01,$11,$21 ; bg2 blue
.db $0F,$00,$10,$30 ; bg3 greyscale
.db $0F,$18,$28,$38 ; sp0 yellow
.db $0F,$14,$24,$34 ; sp1 purple
.db $0F,$1B,$2B,$3B ; sp2 teal
.db $0F,$12,$22,$32 ; sp3 marine

;;;;;;  MAIN FUNCTION ;;;;;;;;;;;;;
main:
	
	;init palette buffer ram
	ldx	#0
init_palette:
	lda	palette_data, x
	sta	pal_buff, x
	inx
	cpx	#32	;32 palette entries total
	bcc	init_palette

	jsr	setup_bkgd


forever:
	jmp	forever




