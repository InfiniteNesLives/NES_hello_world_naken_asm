
isr_RESET:
	sei			;disable interrupts
	cld			;clear decimal mode (not present on NES)
	lda	#0
	sta	PPUCTRL		;disable NMI
	sta	PPUMASK		;disable rendering
	sta	APU_MASTER	;disable APU channels
	sta	APU_DMC_FREQ	;diabble DMC IRQs
	lda	#0x40
	sta	APU_DMC_FRAME	;disable DMC IRQs
	ldx	#STACK_MAX
	txs			;init stack pointer

	;wait for first vblank
	vblank_wait

;clear zp, ram, stack as desired..
;note if this is not done, may need extra vblank waits
.ifdef CLEAR_ALL_RAM
	lda	#0
	ldx	#0
clear_ram:
	sta	$0000, x
	sta	$1000, x
	sta	$2000, x
	sta	$3000, x
	sta	$4000, x
	sta	$5000, x
	sta	$6000, x
	sta	$7000, x
	inx
	bne	clear_ram
.endif

;place all sprites off screen Y=255
;this doesn't actually do anything to sprite data
;it simply initializes SRAM dedicated to be copied to PPU OAM
;to move sprites off screen at first OAM DMA
	lda	#255
	ldx	#0
init_oam:
	sta	oam, x	
	inx
	inx
	inx
	inx
	bne	init_oam

	;wait for second vblank
	vblank_wait

;NES is mostly initialized at this point
	lda	#0b1000_1000	
	;enable NMI, 8x8 sprites, bkgd PT0, spr PT1, vram inc by 1, select NT0
	sta	PPUCTRL	

	jmp	main

