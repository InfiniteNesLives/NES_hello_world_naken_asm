.6502	;tell naken_asm what core to target

;include files macros and defines that would like all code to have
;these asm files can't have actual code

.include "ppu.inc"
.include "apu_io.inc"

.org 0x7FF0
.include "header.asm"

.org 0x0000 	;$0000-07FF RAM 1KB
zp:		;$0000-00FF ZERO PAGE first 256 bytes have optimized short addressing. Sure smells like a 6502!
.include "zp.inc"

.org 0x0100	
ram:
.include "sram.inc"

		;$0800-1FFF mirrors to system RAM

.org 0x2000 	;$2000-2007 PPU registers

.org 0x4000 	;$4000-4017 APU & I/O registers
		;$4018-401F APU & I/O functionality that is normally disabled (test mode)

.org 0x4020 	;$4020-FFFF Cartridge space
		;Any address beyond here is completely dependent on the cartridge hardware/mapper

;.org 0x6000 	;$6000-7FFF 2KByte SRAM aka "WRAM" commonly found on many mappers
;cart_wram:	;Commmonly battery backed to provide save data	

		;$8000-FFFF 32KByte typically PROGRAM ROM space
		;often banked in sections of 8/16/32KBytes depending on mapper
.org 0x8000 	;$8000-9FFF	8KByte PROGRAM ROM
	nop
prg_bank0:

.org 0xA000 	;$A000-BFFF	8KByte PROGRAM ROM
prg_bank1:

.org 0xC000 	;$C000-DFFF	8KByte PROGRAM ROM
prg_bank2:


.org 0xE000 	;$E000-FFFA	8KByte PROGRAM ROM - 6B of vectors often fixed when bank size <32KByte
prg_bank_last:


;init macros
.define CLEAR_ALL_RAM   ;clears all SRAM upon reset
.include "reset.asm"

.include "main.asm"
.include "subroutines.asm"


isr_NMI:
	rti

isr_IRQ:
	rti


last_bank_end:

	;$FFF9 is last byte of program memory

.org 0xFFFA	;$FFFA-FFFF 	3 vectors NMI, RESET, IRQ
dw	isr_NMI		
dw	isr_RESET	
dw	isr_IRQ		

.binfile "example.chr"
