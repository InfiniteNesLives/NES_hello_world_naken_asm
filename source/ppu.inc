;NES PPU registers
;mostly copied from: https://wiki.nesdev.com/w/index.php/PPU_registers

.define	PPUCTRL 	0x2000
	;$2000 	VPHB SINN 	
	;	NMI enable (V), 
	;	PPU master/slave (P), 
	;	sprite height (H), 
	;	background tile select (B), 
	;	sprite tile select (S), 
	;	increment mode (I), 
	;	nametable select (NN)

.define	PPUMASK		0x2001
	;$2001 	BGRs bMmG 	
	;	color emphasis (BGR), 
	;	sprite enable (s), 
	;	background enable (b), 
	;	sprite left column enable (M), 
	;	background left column enable (m), 
	;	greyscale (G)

.define	PPUSTATUS 	0x2002
	;$2002 	VSO- ---- 	
	;	vblank (V), 
	;	sprite 0 hit (S), 
	;	sprite overflow (O), 
	;	read resets write pair for $2005/2006

.define	OAMADDR 	0x2003
	;$2003 	aaaa aaaa 	OAM read/write address

.define	OAMDATA 	0x2004
	;$2004 	dddd dddd 	OAM data read/write

.define	PPUSCROLL 	0x2005
	;$2005 	xxxx xxxx 	fine scroll position (two writes: X, Y)

.define	PPUADDR 	0x2006
	;$2006 	aaaa aaaa 	PPU read/write address (two writes: MSB, LSB)

.define	PPUDATA 	0x2007
	;$2007 	dddd dddd 	PPU data read/write

.define	OAMDMA 		0x2008
	;$4014 	aaaa aaaa 	OAM DMA high address 
	;Writing $xx copies 256 bytes by reading from $xx00-$xxFF and writing to OAMDATA ($2004)
		
;$2008-3FFF mirrors to PPU registers


.macro vblank_wait
.scope
wait:
	bit 	PPUSTATUS
	bpl	wait
.ends
.endm



