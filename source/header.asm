
.define INES_MAPPER  0 ; 0 = NROM
.define	INES_MIRROR  1 ; 0 = horizontal mirroring, 1 = vertical mirroring
.define	INES_SRAM    0 ; 1 = battery backed SRAM at $6000-7FFF

.ascii 'N', 'E', 'S', $1A ; ID
.db $02 ; 16k PRG bank count
.db $01 ; 8k CHR bank count
.db INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.db (INES_MAPPER & %11110000)
.db $0, $0, $0, $0, $0, $0, $0, $0 ; padding
