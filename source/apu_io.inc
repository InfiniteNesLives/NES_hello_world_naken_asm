;NES APU registers
;mostly copied from: https://wiki.nesdev.com/w/index.php/APU_registers

;Addr	7654.3210 	Function

;-------------------------
;Pulse 1 channel (write)
;-------------------------
.define	APU_SQ1_VOL 	0x4000
	;$4000 	DDLC NNNN 	
	;Duty, loop envelope/disable length counter, constant volume, envelope period/volume

.define	APU_SQ1_SWEEP 	0x4001
	;$4001 	EPPP NSSS 	Sweep unit: enabled, period, negative, shift count

.define	APU_SQ1_LO 	0x4002
	;$4002 	LLLL LLLL 	Timer low

.define	APU_SQ1_HI 	0x4003
	;$4003 	LLLL LHHH 	Length counter load, timer high (also resets duty and starts envelope)
	 

;-------------------------
;Pulse 2 channel (write)
;-------------------------
.define	APU_SQ2_VOL 	0x4004
	;$4004 	DDLC NNNN 	Duty, loop envelope/disable length counter, constant volume, envelope period/volume

.define	APU_SQ2_SWEEP 	0x4005
	;$4005 	EPPP NSSS 	Sweep unit: enabled, period, negative, shift count

.define	APU_SQ2_LO 	0x4006
	;$4006 	LLLL LLLL 	Timer low

.define	APU_SQ2_HI 	0x4007
	;$4007 	LLLL LHHH 	Length counter load, timer high (also resets duty and starts envelope)
		 

;-------------------------
;Triangle channel (write)
;-------------------------
.define	APU_TRI_LINEAR 	0x4008
	;$4008 	CRRR RRRR 	Length counter disable/linear counter control, linear counter reload value

.define	APU_TRI_LO 	0x400A
	;$400A 	LLLL LLLL 	Timer low

.define	APU_TRI_HI 	0x400B
	;$400B 	LLLL LHHH 	Length counter load, timer high (also reloads linear counter)
			 

;-------------------------
;Noise channel (write)
;-------------------------
.define	APU_NOISE_VOL 	0x400C
	;$400C 	--LC NNNN 	Loop envelope/disable length counter, constant volume, envelope period/volume

.define	APU_NOISE_LO 	0x400E
	;$400E 	L--- PPPP 	Loop noise, noise period

.define	APU_NOISE_HI 	0x400F
	;$400F 	LLLL L--- 	Length counter load (also starts envelope)
				 
;-------------------------
;DMC channel (write)
;-------------------------
.define	APU_DMC_FREQ 	0x4010
	;$4010 	IL-- FFFF 	IRQ enable, loop sample, frequency index

.define	APU_DMC_RAW 	0x4011
	;$4011 	-DDD DDDD 	Direct load

.define	APU_DMC_START 	0x4012
	;$4012 	AAAA AAAA 	Sample address %11AAAAAA.AA000000

.define	APU_DMC_LEN 	0x4013
	;$4013 	LLLL LLLL 	Sample length %0000LLLL.LLLL0001
					 

;-------------------------
;APU master (some read, some write)
;-------------------------
.define	APU_MASTER 	0x4015
	;WRITE
	;$4015 	---D NT21 	Control: DMC enable, length counter enables: noise, triangle, pulse 2, pulse 1 (write)
	;READ
	;$4015 	IF-D NT21 	Status: DMC interrupt, frame interrupt, length counter status: 
	;			noise, triangle, pulse 2, pulse 1 (read)

.define	APU_DMC_FRAME 	0x4017
	;WRITE
	;$4017 	SD-- ---- 	Frame counter: 5-frame sequence, disable frame interrupt (write) 

;-------------------------
;NES I/O
;-------------------------
;Addr 	7654.3210 	Function
;$4016 (W) 	----.---A 	Output data (strobe) to both controllers.
;----.-CBA 	Output data to the expansion port.
;$4016 (R) 	---4.3--0 	Read data from controller port #1.
;---4.3210 	Read data from expansion port.
;$4017 (R) 	---4.3--0 	Read data from controller port #2.
;---4.3210 	Read data from expansion port.
;-------------------------
;Famicom I/O
;-------------------------
;Addr 	7654.3210 	Function
;$4016 (W) 	----.---A 	Output strobe to both controllers.
;----.-CBA 	Output data to the expansion port.
;$4016 (R) 	----.-M-0 	Read data from controller #1 and microphone state from controller #2
;----.--1- 	Read data from expansion port.
;$4017 (R) 	----.---0 	Read data from controller #2.
;---4.3210 	Read data from expansion port. 

.define	IO_JOY1		0x4016
	;READ & WRITE

.define	IO_JOY2		0x4017
	;READ ONLY (APU_DMC_FRAME as write)


